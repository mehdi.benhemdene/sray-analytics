import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LogsComponent } from './logs/logs.component';
import { MatTabsModule } from '@angular/material/tabs';
import { SharedComponentsModule } from '../shared/shared-components/shared-components.module';
import { MatSnackBarModule } from '@angular/material/snack-bar';

@NgModule({
  declarations: [LogsComponent],
  imports: [
    CommonModule,
    MatTabsModule,
    SharedComponentsModule,
    MatSnackBarModule
  ],
  exports: [LogsComponent]
})
export class ComponentsModule {}
