import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogsComponent } from './logs.component';
import { MatTabsModule } from '@angular/material/tabs';
import { SharedComponentsModule } from '../../shared/shared-components/shared-components.module';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { HttpClientModule } from '@angular/common/http';
import { AppModule } from '../../app.module';
import { LogService } from '../../shared/services/log.service';
import { PieChartComponent } from '../../shared/shared-components/pie-chart/pie-chart.component';
import { LineChartComponent } from '../../shared/shared-components/line-chart/line-chart.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('LogsComponent', () => {
  let component: LogsComponent;
  let fixture: ComponentFixture<LogsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LogsComponent, PieChartComponent, LineChartComponent],
      imports: [
        BrowserAnimationsModule,
        HttpClientModule,
        MatTabsModule,
        MatSnackBarModule
      ],
      providers: [LogService]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
