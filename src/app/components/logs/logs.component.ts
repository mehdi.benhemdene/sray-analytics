import { Component, OnInit } from '@angular/core';
import { LogService } from '../../shared/services/log.service';
import { IData } from '../../shared/models/IData';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-logs',
  templateUrl: './logs.component.html',
  styleUrls: ['./logs.component.css']
})
export class LogsComponent implements OnInit {
  constructor(private logService: LogService, private snackBar: MatSnackBar) {}
  pieChartData: Array<IData>;
  lineChartData: Array<IData>;
  loaded = false;
  ngOnInit() {
    this.logService.getLogs().subscribe(
      resp => {
        this.pieChartData = resp.sectorsOfInterest.map(el => {
          return { label: el.sector, value: el.visited };
        });
        this.lineChartData = resp.uniqueLoggedInUsersPerDay.map(el => {
          return { label: el.date, value: el.count };
        });

        this.loaded = true;
      },
      err => {
        this.snackBar.open('An error occured', 'Close', { duration: 1000 });
      }
    );
  }
}
