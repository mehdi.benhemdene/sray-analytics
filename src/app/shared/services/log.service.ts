import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LogService {
  constructor(private http: HttpClient) {}
  ssss;

  getLogs() {
    return this.http
      .get(
        'https://s3.eu-west-2.amazonaws.com/sample-sray-logs-coding-interview/sray-logs.json'
      )
      .pipe(
        map((resp: any) => {
          const { logs } = resp;

          return {
            uniqueLoggedInUsersPerDay: this.getUniqueLoggedInUsers(logs),
            sectorsOfInterest: this.getSectorsOfInterest(logs)
          };
        })
      );
  }

  private getUniqueLoggedInUsers(logs) {
    return logs
      .filter(log => log.action == 'LOAD')
      .reduce((accumulator, log) => {
        const date = new Date(log.date);
        const dateString = `${date.getDate()}/${date.getMonth() +
          1}/${date.getFullYear()}`;
        const existing = accumulator.filter(el => el.date == dateString);
        if (existing.length == 0) {
          accumulator.push({
            date: dateString,
            userIds: new Set([log.user_id])
          });
        } else {
          existing[0].userIds.add(log.user_id);
        }
        return accumulator;
      }, [])
      .map(el => {
        el.count = el.userIds.size;
        delete el.userIds;
        return el;
      });
  }

  private getSectorsOfInterest(logs) {
    return logs
      .filter(log => log.action === 'FILTER' && log.payload.sector !== null)
      .reduce((accumulator, log) => {
        const sectors = log.payload.sector;
        sectors.forEach(sector => {
          const existing = accumulator.filter(el => el.sector == sector);
          if (existing.length == 0) {
            accumulator.push({ sector, visited: 1 });
          } else {
            existing[0].visited++;
          }
        });
        return accumulator;
      }, []);
  }
}
