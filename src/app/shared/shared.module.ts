import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ServicesModule } from './services/services.module';
import { SharedComponentsModule } from './shared-components/shared-components.module';
@NgModule({
  declarations: [],
  imports: [CommonModule, ServicesModule]
})
export class SharedModule {}
