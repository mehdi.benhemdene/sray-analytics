import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PieChartComponent } from './pie-chart/pie-chart.component';
import { MatBadgeModule } from '@angular/material/badge';
import { LineChartComponent } from './line-chart/line-chart.component';

@NgModule({
  declarations: [PieChartComponent, LineChartComponent],
  imports: [CommonModule, MatBadgeModule],
  exports: [PieChartComponent,LineChartComponent]
})
export class SharedComponentsModule {}
