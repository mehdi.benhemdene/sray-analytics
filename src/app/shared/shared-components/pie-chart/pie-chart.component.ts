import {
  Component,
  Input,
  ViewChild,
  ElementRef,
  AfterViewInit
} from '@angular/core';
import * as D3 from 'd3';
import { MaterialColors } from '../../material-colors';
import { IData } from '../../models/IData';

@Component({
  selector: 'pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.css']
})
export class PieChartComponent implements AfterViewInit {
  @ViewChild('containerPieChart') element: ElementRef;
  @ViewChild('tooltip') tooltip: ElementRef;

  @Input()
  private width = window.innerWidth / 2;
  @Input()
  private height = window.innerHeight / 2;
  @Input('data')
  private data: Array<IData> = [];

  legend = [];
  private htmlElement: HTMLElement;
  private radius = 0;
  private host: D3.Selection;
  private svg: D3.Selection;
  constructor() {}

  ngAfterViewInit() {
    this.htmlElement = this.element.nativeElement;
    this.host = D3.select(this.htmlElement);
    this.radius = Math.min(this.width, this.height) / 2;
    this.buildSVG();
    this.buildPie();
  }

  private buildSVG() {
    this.host.html('');
    this.svg = this.host
      .append('svg')
      .attr('viewBox', `0 0 ${this.width} ${this.height}`)
      .style('width', `${this.width}px`)
      .style('height', `${this.height}px`)
      .append('g')
      .attr('transform', `translate(${this.width / 2},${this.height / 2})`);
  }
  private buildPie() {
    const pie = D3.pie;
    const data = pie()(this.data.map(el => el.value));

    const arcSelection = this.svg
      .selectAll('.arc')
      .data(data)
      .enter()
      .append('g')
      .attr('class', 'arc');

    this.populatePie(arcSelection);
  }
  private populatePie(arcSelection: D3.Selection<D3.layout.pie.Arc>): void {
    const outerRadius = this.radius - 10;
    const tooltip = D3.select(this.tooltip.nativeElement);
    const arc = D3.arc<D3.layout.pie.Arc>()
      .outerRadius(outerRadius)
      .innerRadius(0);
    arcSelection
      .append('path')
      .attr('d', arc)
      .attr('fill', (datum, index) => {
        const color = MaterialColors.getColor(index);
        this.legend.push({ color, label: this.data[index].label });
        return color;
      })
      .on('mouseenter', d => {
        const x = D3.event.pageX;
        const y = D3.event.pageY;
        tooltip
          .transition()
          .duration(1000)
          .style('opacity', 0.9);
        tooltip.html(`${this.data[d.index].label} - searched ${this.data[d.index].value} times`);
      })
      .on('mouseleave', d => {
        tooltip
          .transition()
          .duration(1000)
          .style('opacity', 0);
      });
  }
}
