import {
  Component,
  Input,
  ViewChild,
  ElementRef,
  AfterViewInit
} from '@angular/core';
import * as D3 from 'd3';
import { MaterialColors } from '../../material-colors';
import { IData } from '../../models/IData';

@Component({
  selector: 'line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.css']
})
export class LineChartComponent implements AfterViewInit {
  @ViewChild('containerLineChart') element: ElementRef;
  @ViewChild('tooltip') tooltip: ElementRef;

  @Input()
  private width = window.innerWidth / 2;
  @Input()
  private height = window.innerHeight / 2;
  @Input('data')
  private data: Array<IData> = [];

  legend = [];
  private htmlElement: HTMLElement;
  private radius = 0;
  private host: D3.Selection;
  private svg: D3.Selection;
  constructor() {}

  ngAfterViewInit() {
    this.htmlElement = this.element.nativeElement;
    this.host = D3.select(this.htmlElement);
    this.buildSVG();
    this.buildLineChart();
  }

  private buildSVG() {
    this.host.html('');
    const margin = { top: 50, right: 50, bottom: 50, left: 50 };
    this.svg = this.host
      .append('svg')

      .attr('width', this.width + margin.left + margin.right)
      .attr('height', this.height + margin.top + margin.bottom)
      .append('g')
      .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');
  }
  private buildLineChart() {
    // set the dimensions and margins of the graph
    const margin = { top: 30, right: 20, bottom: 50, left: 70 },
      width = 960 - margin.left - margin.right,
      height = 500 - margin.top - margin.bottom;

    // parse the date / time
    const parseTime = D3.timeParse('%e %m %Y');

    // set the ranges
    const x = D3.scaleTime().range([0, width]);
    const y = D3.scaleLinear().range([height, 0]);
    const tooltip = D3.select(this.tooltip.nativeElement);
    // define the line
    const valueline = D3.line()
      .x(function(d) {
        return x(d.date);
      })
      .y(function(d) {
        return y(d.value);
      })
      .curve(D3.curveMonotoneX); // apply smoothing to the line;
    const dataSet = this.data.map(el => {
      return { value: el.value, date: parseTime(el.label.replace(/\//g, ' ')) };
    });
    x.domain(
      D3.extent(dataSet, function(d) {
        return d.date;
      })
    );
    y.domain([
      0,
      D3.max(dataSet, function(d) {
        return d.value;
      })
    ]);

    // Add the valueline path.
    this.svg
      .append('path')
      .data([dataSet])
      .attr('class', 'line')
      .attr('d', valueline);

    // Add the x Axis
    this.svg
      .append('g')
      .attr('transform', 'translate(0,' + height + ')')
      .call(D3.axisBottom(x));

    // text label for the x axis
    this.svg
      .append('text')
      .attr(
        'transform',
        'translate(' + width / 2 + ' ,' + (height + margin.top + 20) + ')'
      )
      .style('text-anchor', 'middle')
      .text('Date');

    // Add the y Axis
    this.svg.append('g').call(D3.axisLeft(y));

    // text label for the y axis
    this.svg
      .append('text')
      .attr('transform', 'rotate(-90)')
      .attr('y', 0 - margin.left)
      .attr('x', 0 - height / 2)
      .attr('dy', '1em')
      .style('text-anchor', 'middle')
      .text('Value');

    this.svg
      .selectAll('.dot')
      .data(dataSet)
      .enter()
      .append('circle') // Uses the enter().append() method
      .attr('class', 'dot') // Assign a class for styling
      .attr('cx', function(d, i) {
        return x(d.date);
      })
      .attr('cy', function(d) {
        return y(d.value);
      })
      .attr('r', 5)
      .on('mouseover', function(d) {
        tooltip
          .transition()
          .duration(200)
          .style('opacity', 0.9);
        tooltip
          .html(
            d.date.getDate() +
              '/' +
              (d.date.getMonth() + 1) +
              '<br/>' +
              d.value +
              ' Users'
          )
          .style('left', D3.event.pageX + 'px')
          .style('top', D3.event.pageY - 200 + 'px');
      })

      .on('mouseout', function() {});
  }
}
