# Sray-Analytics

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.1.4.

## Development server

If you haven't yet, you should start by running `npm install` to install the project's depedencies

And the angular CLI :  `npm install -g @angular/cli`


Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


## Project structure

| Directory        |                                 Description                                 |
| ---------------- | :-------------------------------------------------------------------------: |
| `app/components` |              Angular module containing all the app components               |
| `app/shared`     | Contains shared resources between components (Services, Components, models) |
